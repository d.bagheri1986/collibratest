package main.collibra.com;

import main.collibra.com.graphimpl.DijkstraAlgorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

public class CollibraServer {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private static GraphProcessor graphProcessor;

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        graphProcessor = new GraphProcessor();
        while (true)
            new EchoClientHandler(serverSocket.accept()).start();
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();
    }

    private static class EchoClientHandler extends Thread {
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;

        public EchoClientHandler(Socket socket) {
            this.clientSocket = socket;
        }

        public void run() {
            String lastWord = "";
            Instant start = null;
            Instant finish;
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                out.println("HI, I’M " + UUID.randomUUID());
                start = Instant.now();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            String inputLine = "";
            while (true) {
                try {
                    if ((inputLine = in.readLine()) == null)
                        break;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (inputLine.contains("HI, I’M")) {
                    lastWord = inputLine.substring(inputLine.lastIndexOf(" ") + 1);
                    out.println("HI " + lastWord);
                } else if ("BYE MATE!".equals(inputLine)) {
                    finish = Instant.now();
                    long timeElapsed = 0;
                    if (start != null && finish != null) {
                        timeElapsed = Duration.between(start, finish).toMillis();
                    }
                    out.println("BYE " + lastWord + "WE SPOKE FOR: " + timeElapsed + " ms");
                    break;
                } else if (inputLine.contains("ADD NODE")){
                    String nodeName = inputLine.substring(inputLine.lastIndexOf(" ") + 1);
                    String result = graphProcessor.getGraph().addVertex(nodeName);
                    out.println(result);
                } else if (inputLine.contains("ADD EDGE")){
                    String str[] = inputLine.split(" ");
                    String sourceName =str[str.length - 3];
                    String destName = str[str.length -2];
                    int weight = Integer.parseInt(str[str.length -1]);
                    String result = graphProcessor.getGraph().addEdge(sourceName, destName, weight);
                    out.println(result);
                } else if (inputLine.contains("REMOVE NODE")){
                    String nodeName = inputLine.substring(inputLine.lastIndexOf(" ") + 1);
                    String result = graphProcessor.getGraph().removeVertex(nodeName);
                    out.println(result);
                } else if (inputLine.contains("REMOVE EDGE")) {
                    String str[] = inputLine.split(" ");
                    String sourceName =str[str.length - 2];
                    String destName = str[str.length -1];
                    String result = graphProcessor.getGraph().removeEdge(sourceName, destName);
                    out.println(result);
                } else if (inputLine.contains("SHORTEST PATH")){
                    String str[] = inputLine.split(" ");
                    String sourceName =str[str.length - 2];
                    String destName = str[str.length -1];
                    DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graphProcessor.getGraph());
                    int shortestPath = dijkstra.getshortestDistanceOfTwoNodes(sourceName, destName);
                    out.println(shortestPath);
                }
                else {
                    out.println("SORRY, I DIDN’T UNDERSTAND THAT");
                }
            }

            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.close();
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
