package main.collibra.com.graphimpl;

import java.util.ArrayList;
import java.util.List;

public class Graph {

    private List<Vertex> vertexes;
    private List<Edge> edges;

    public Graph(List<Vertex> vertexes, List<Edge> edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

    public Graph(){
        this.vertexes = new ArrayList<>();
        this.edges = new ArrayList<>();
    }

    public void setVertexes(List<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public List<Vertex> getVertexes() {
        return vertexes;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public String addVertex(String label) {
        Vertex v = new Vertex(label, label);
        if (!vertexes.contains(v)) {
            vertexes.add(new Vertex(label, label));
            return "NODE ADDED";
        }
        return "ERROR: NODE ALREADY EXISTS";
    }

    public Vertex getVertexByLabel(String label){
        return vertexes.stream()
            .filter(vertex -> label.equals(vertex.getName()))
            .findAny()
            .orElse(null);
    }

    public String addEdge(String source, String dest, int weight) {
        String edgeId = source + dest;
        Vertex sourceVertex = this.getVertexByLabel(source);
        Vertex destVertex = this.getVertexByLabel(dest);
        if (sourceVertex == null || destVertex == null) {
            return "ERROR: NODE NOT FOUND";
        }

        Edge edge = new Edge(edgeId,this.getVertexByLabel(source), this.getVertexByLabel(dest), weight );
        edges.add(edge);
        return "EDGE ADDED";
    }

    public String removeVertex(String label){
        if(vertexes.removeIf(vertex -> vertex.getName().equals(label))){
            edges.removeIf(edge -> edge.getSource().getName().equals(label));
            edges.removeIf(edge -> edge.getDestination().getName().equals(label));
            return "NODE REMOVED";
        };

       return "ERROR: NODE NOT FOUND";
    }

    public String removeEdge(String source, String dest) {
        Vertex sourceVertex = new Vertex(source, source);
        Vertex destVertex = new Vertex(dest, dest);
        if (vertexes.contains(sourceVertex) && vertexes.contains(destVertex)) {
            edges.removeIf(edge -> (edge.getSource().getName().equals(source) && edge.getSource().getName().equals(dest)));
            return "EDGE REMOVED";
        }

        return "ERROR: NODE NOT FOUND";
    }


}
