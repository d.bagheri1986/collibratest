package main.collibra.com;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Application started");
        CollibraServer server=new CollibraServer();
        server.start(50000);
    }
}
