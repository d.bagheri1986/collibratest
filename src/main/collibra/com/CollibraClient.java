package main.collibra.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class CollibraClient {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public String startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        return in.readLine();
    }

    public String sendMessage(String msg) throws IOException {
        out.println(msg);
        String resp = in.readLine();

        if ("BYE MATE!".equals(msg))
            this.stopConnection();

        return resp;
    }

    public void stopConnection() throws IOException {
        clientSocket.close();
    }
}
