package test;


import main.collibra.com.CollibraClient;


import java.io.IOException;

public class ServerTest {

    int port = 50000;

    @org.junit.Test
    public void TestTwoClientsSimultanously() throws IOException {
        CollibraClient client1 = new CollibraClient();
        String atStart = client1.startConnection("127.0.0.1", port);
        System.out.println(atStart);
        String msg1_1 = client1.sendMessage("HI, I’M john");
        System.out.println(msg1_1);

        String msg1_10 = client1.sendMessage("ADD NODE node_0");
        System.out.println(msg1_10);
        String msg1_11 = client1.sendMessage("ADD NODE node_1");
        System.out.println(msg1_11);

        String msg1_12 = client1.sendMessage("ADD NODE node_0");
        System.out.println(msg1_12);

        String msg1_13 = client1.sendMessage("ADD EDGE node_0 node_1 5");
        System.out.println(msg1_13);
        String msg1_2 = client1.sendMessage("BYE MATE!");
        System.out.println(msg1_2);


        CollibraClient client2 = new CollibraClient();
        String atStart_2 = client2.startConnection("127.0.0.1", port);
        System.out.println(atStart_2);
        String msg1 = client2.sendMessage("HI, I’M David");
        System.out.println(msg1);
        String msg2 = client2.sendMessage("BYE MATE!");
        System.out.println(msg2);

    }

}


